package am.matveev.dance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ModuleDanceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ModuleDanceApplication.class, args);
    }
}
